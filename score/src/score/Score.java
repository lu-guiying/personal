package score;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Score {
	private static double BEFORE;//课堂自测
	private static double BASE;//课堂完成部分
	private static double TEST;//课堂小测部分
	private static double PROGRAM;//编程题
	private static double ADD;//附加题
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		//得到各部分的总经验值
		getTotleScoreMap();
		//计算我的成绩
		double score = myScore();
		//输出
		System.out.println(String.format("%.2f", score));
	}
	//给所有的全局变量赋上对应配置文件里的值
	public static void getTotleScoreMap(){
		//从配置文件中加载配置项：使用Properties的load()方法
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("total.properties"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//读取配置项中的结果：使用Properties的getProperty()方法
		BEFORE = Double.parseDouble(prop.getProperty("before"));
		BASE = Double.parseDouble(prop.getProperty("base"));
		TEST = Double.parseDouble(prop.getProperty("test"));
		PROGRAM = Double.parseDouble(prop.getProperty("program"));
		ADD = Double.parseDouble(prop.getProperty("add"));
	}
	//获取到经验值并转为double
	//参数1：带有文本内容为经验值的字符串，参数2：开始截取字符串的位置
	public static Double partScore(String scoreString,int index) {
		double score=0.0;
		//截取到经验值并转为double
		score = Double.parseDouble(scoreString.substring(index, scoreString.length()-3));
		return score;
	}
	public static Double myScore() throws IOException {
		//初始化总成绩
		double score = 0.0;
		//初始化我的附加题总经验
		double myAdd = 0.0;
		//初始化我的编程题总经验
		double myProgram = 0.0;
		//我的课前自测总经验
		double myBefore = 0.0;
		//double huPing = 0.0;
		//解析html文件
		File sFile = new File("small.html");
		File aFile = new File("all.html");
		Document small = Jsoup.parse(sFile,"UTF-8");
		Document all = Jsoup.parse(aFile,"UTF-8");
		//获取到所有活动的div
		Elements activDivs = small.getElementsByClass("interaction-row");
		activDivs.addAll(all.getElementsByClass("interaction-row"));
		//计算
		for(int i = 0;i < activDivs.size();i++) {
			//课堂完成部分base
			if (activDivs.get(i).child(1).child(0).child(1).text().indexOf("课堂完成部分") != -1) {
				//参加了活动
				if (activDivs.get(i).child(1).child(2).child(0).child(6).text().indexOf("已参与") != -1) {
					//根据权重计算课堂完成部分的成绩
					score += (partScore(activDivs.get(i).child(1).child(2).child(0).child(7).text(),0)/BASE)*95*0.3;
				}
			}
			//课堂小测test
			else if (activDivs.get(i).child(1).child(0).child(1).text().indexOf("课堂小测") != -1) {
				//参加了活动
				if (activDivs.get(i).child(1).child(2).child(0).child(6).text().indexOf("已参与") != -1) {
					//如果有互评则计算互评成绩
					if(activDivs.get(i).child(1).child(2).child(0).text().indexOf("互评") != -1) {
						//判断是否参加互评
						if(activDivs.get(i).child(1).child(2).child(0).child(9).toString().contains("color:#8FC31F")) {
							score += ((partScore(activDivs.get(i).child(1).child(2).child(0).child(9).text(),3))/TEST)*100*0.2;
						}
					}
					//根据权重计算课堂小测的成绩
					score += ((partScore(activDivs.get(i).child(1).child(2).child(0).child(7).text(),0))/TEST)*100*0.2;
				}
			}
			//附加题add
			else if (activDivs.get(i).child(1).child(0).child(1).text().indexOf("附加题") != -1) {
				//参加了活动
				if (activDivs.get(i).child(1).child(2).child(0).child(6).text().indexOf("已参与") != -1) {
					//计算附加题的总经验值
					myAdd += (partScore(activDivs.get(i).child(1).child(2).child(0).child(7).text(),0)/ADD)*100;
				}
			}
			//编程题program
			else if (activDivs.get(i).child(1).child(0).child(1).text().indexOf("编程题") != -1) {
				//参加了活动
				if (activDivs.get(i).child(1).child(2).child(0).child(6).text().indexOf("已参与") != -1) {
					//根据计算编程题的总经验值
					myProgram += (partScore(activDivs.get(i).child(1).child(2).child(0).child(7).text(),0)/PROGRAM)*100;
				}
			}
			//课前自测before
			else if(activDivs.get(i).child(1).child(0).child(1).text().indexOf("课前自测") != -1) {
				//参加了活动
				if (activDivs.get(i).child(1).child(2).child(0).child(10).toString() != null) {
					//得到经验值文本开始的前一个位置
					int index = activDivs.get(i).child(1).child(2).child(0).child(10).toString().indexOf('>');
					//string为整个有经验值的spans
					String string = activDivs.get(i).child(1).child(2).child(0).child(10).toString();
					//计算总的课前自测成绩
					myBefore += Double.parseDouble(string.substring(index+1,string.length()-10));
				}
			}
		}
		//附加题的另外计算:得到总的成绩后再按规则计算
		if (myAdd >= 90.0) {
			myAdd = 90.0*0.15;
		}else {
			myAdd = myAdd*0.9*0.15;
		}
		//编程题的另外计算:得到总的成绩后再按规则计算
		if (myProgram >= 95.0) {
			myProgram = 95.0*0.1;
		}else {
			myProgram = myProgram*0.95*0.1;
		}
		//课前自测:得到总的成绩后再按权重计算
		myBefore = myBefore/BEFORE*100*0.25;
		//我的成绩
		score = score + myAdd + myProgram + myBefore;
		return score;
	}
}
