// Date 对象换为天数，小时数，分钟数
function turnToDays (dateObj) {
    var dayStamp = dateObj.getTime() / 1000 / 60 / 60 / 24;
    return Math.floor(dayStamp);
}
function turnToHours (dateObj) {
    var hourStamp = dateObj.getTime() / 1000 / 60 / 60;
    return Math.floor(hourStamp);
}
function turnToMinutes (dateObj) {
    var minuteStamp = dateObj.getTime() / 1000 / 60;
    return Math.floor(minuteStamp);
}
// 初始化日期，时间 easyui
function initDateTime (elements) {
    var tomorrow = new Date(new Date().getTime() + 86400000);
    var month = tomorrow.getMonth() < 9 ? '0' + (tomorrow.getMonth() + 1).toString() : (tomorrow.getMonth() + 1).toString();
    var date = tomorrow.getDate() < 10 ? '0' + tomorrow.getDate().toString() : tomorrow.getDate().toString();
    var dateString = tomorrow.getFullYear() + '-' + month + '-' + date;
    elements.dateBox.datebox('setValue', dateString); // 设置日期

    elements.hourBox.combobox('loadData', easyuiReloadHours(elements.dateBox)); // 小时范围
    elements.hourBox.combobox('setValue', 8); // 设置小时
    elements.minuteBox.combobox('loadData', easyuiReloadMinutes(elements.dateBox, 8)); // 分钟范围
    var minute = elements.minuteBox.combobox('getValue'); // 获取分钟
}
/**
 * 转换为 Date 对象
 * 参数： yyyy-MM-DD 格式的日期； 整数小时； 整数分钟
 */
function turnToDateObj (date, hour, minute) {
    if (!date || !hour || !minute) {
        return new Date(new Date().getTime() + 86400000);
    }
    var dateObj = new Date();
    var dateArray = date.split('-');
    dateObj.setFullYear(Number(dateArray[0]), Number(dateArray[1]) - 1, Number(dateArray[2]));
    dateObj.setHours(Number(hour), Number(minute), 0, 0);
    return dateObj;
}
// 定时器  刷新时间选择期的最小限制
var timer = {
    timeUpdateEvent: function(){

    },
    initElement: function(elements){
        this.dateBox = elements.date;
        this.hourBox = elements.hour;
        this.minuteBox = elements.minute;
    },
    dateBox: null,
    hourBox: null,
    minuteBox: null,
    timerId: 0,
    lastTime: null,  // 上一秒的日期
    stop: function(){
        if (this.timerId === 0) return;
        clearInterval(this.timerId);
        this.timerId = 0;
    },
    run: function(){
        if (this.dateBox === null || this.hourBox === null || this.minuteBox === null) return;
        if (this.timerId !== 0) return;
        var self = this;
        this.timerId = setInterval(function(){
            var timeChange = false;
            var thisTime = new Date();
            self.lastTime = self.lastTime || thisTime;
            // 用户预设的日期，时间
            var userSelectedData = {
                date: self.dateBox.datebox('getValue'),
                hours: self.hourBox.combobox('getValue'),
                minutes: self.minuteBox.combobox('getValue')
            };
            var userDateArray = userSelectedData.date.split('-');
            // 定义一个用户所选的 Date 对象
            var userTime = new Date();
            userTime.setFullYear(userDateArray[0], Number(userDateArray[1]) - 1, userDateArray[2]);
            userTime.setHours(userSelectedData.hours, userSelectedData.minutes, 0, 0);

            // 刷新列表，控制可选项目
            // 分钟
            if (turnToMinutes(self.lastTime) != turnToMinutes(thisTime)) {
                timeChange = true;
                // 日期
                if (thisTime.getMinutes() >= 59 && thisTime.getHours() >= 23 ) {
                    // 过滤日期
                    self.dateBox.datebox().datebox('calendar').calendar({
                        validator: function(date){
                            return easyuiValidator(date);
                        }
                    });
                    // 用户的日期大于当前日期 -> 显示用户的日期（设置默认值）
                    var dateValue = '';
                    if (turnToDays(userTime) > turnToDays(thisTime)) {
                        dateValue = userSelectedData.date;
                    } else {
                        var minEnableDate = new Date(thisTime.getTime() + 86400000);
                        var tmp_month = minEnableDate.getMonth() < 9 ? '0' + (minEnableDate.getMonth() + 1).toString() : (minEnableDate.getMonth() + 1).toString();
                        var tmp_date = minEnableDate.getDate() < 10 ? '0' + minEnableDate.getDate().toString() : minEnableDate.getDate().toString();
                        dateValue = minEnableDate.getFullYear() + '-' + tmp_month + '-' + tmp_date;
                    }
                    self.dateBox.datebox('setValue', dateValue);
                }
                // 小时
                if (thisTime.getMinutes() >= 59) {
                    self.hourBox.combobox('loadData', easyuiReloadHours(self.dateBox)); // 重载小时列表
                    // 用户设置的小时大于当前的小时（设置默认值）
                    if (turnToHours(userTime) > turnToHours(thisTime)) {
                        self.hourBox.combobox('setValue', userSelectedData.hours);
                    }
                }
                var factHours = self.hourBox.combobox('getValue');
                self.minuteBox.combobox('loadData', easyuiReloadMinutes(self.dateBox, factHours)); // 重载分钟列表
                // 用户设置时间大于当前的时间（设置默认值）
                if (turnToMinutes(userTime) > turnToMinutes(thisTime)) {
                    self.minuteBox.combobox('setValue', userSelectedData.minutes);
                }
            }
            if (timeChange && typeof self.timeUpdateEvent === 'function') {
                var date = self.dateBox.datebox('getValue');
                var hour = self.hourBox.combobox('getValue');
                var minute = self.minuteBox.combobox('getValue');
                self.timeUpdateEvent(date, hour, minute);
            }
            self.lastTime = thisTime;
        }, 1000);
    }
};
